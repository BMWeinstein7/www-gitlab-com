(function() {
  //TweenMax.staggerTo('.hero-item', 1.6, {ease: Power4.easeOut, y: 0, opacity: 1}, 0.1);
  var videoThumb = document.getElementById('hero-video-thumb');
  var nav = document.getElementById('main-nav');
  var transparent = false;

  var heroVideoContainer = document.getElementById('hero-video-container');
  var heroVideo = document.getElementById('hero-video');
  var videoSource = 'https://www.youtube.com/embed/yjxrBSllNGo?autoplay=1&rel=0';

  // functions to show and hide the explainer video
  function showVideo() {
    heroVideoContainer.style.display = 'flex';
    heroVideo.src = videoSource;
  }

  function hideVideo() {
    heroVideoContainer.style.display = 'none';
    heroVideo.src = '';
  }

  function stopProp(event) {
    event.stopPropagation();
  }

  //videoThumb.addEventListener('click', showVideo);
  //heroVideoContainer.addEventListener('click', hideVideo);
  //heroVideo.addEventListener('click', stopProp);
})();
