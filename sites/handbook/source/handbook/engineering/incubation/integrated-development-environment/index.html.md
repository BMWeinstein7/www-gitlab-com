---
layout: handbook-page-toc
title: Integrated Development Environment Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Integrated Development Environment Single-Engineer Group

The Integrated Development Environment SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

The goal of this SEG is to explore how we can increase customer usage and company revenue by providing an alternative to GitPod or Codespaces directly within the GitLab Web IDE.  Although GitLab [integrates with GitPod](https://www.gitpod.io/blog/gitlab-integration/), there may be opportunities to provide convenience to customers by delivering a cloud hosted development environment natively within GitLab.com.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329598](https://gitlab.com/gitlab-org/gitlab/-/issues/329598)
