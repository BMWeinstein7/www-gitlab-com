---
layout: markdown_page
title: Personal Data Requests
---

Under certain privacy laws, such as GDPR and CCPA, some GitLab users have the right to request that GitLab provides you with information about the personal data GitLab has about you. They also have the right to request complete removal of your GitLab.com account in accordance with these laws. Though the laws apply only to individuals in certain geographic areas, GitLab accepts requests for data access and deletion from all of our users.

To submit these requests, please provide your information using this [Personal Data Request form](https://support.gitlab.io/account-deletion/).

Please note that:

- we may request additional information in order to ensure proper verification of identity
- in many circumstances, we will not be able to provide a full export of associated repositories
- (if requesting account deletion) any groups that you're a sole owner in will be deleted, along with any projects contained therein

Once you've submitted your request, we'll have the various teams review our systems for your personal data and send you final confirmation after this process is complete.
